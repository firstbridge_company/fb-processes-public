/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.fb.process.examples;

import io.firstbridge.process.utils.JreFinder;
import io.firstbridge.process.utils.JreInfo;
import io.firstbridge.process.utils.OsUtils;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Tries to find installed JREs
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class FindJREs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Set<String> searchDirs = new HashSet<>();
        searchDirs.addAll(Arrays.asList(args));
        //add possible locations of "java" executable
        searchDirs.add("../../jre/bin");
        searchDirs.add("../../../jre/bin");
        //add system PATH variable


        JreFinder finder = new JreFinder();
        searchDirs.addAll(finder.proposeSearchDirs());
        System.out.println("=== Following JREs found: ====");
        for(JreInfo ji: finder.findWithInfo(searchDirs)){
            System.out.println("Path: "+ji.getAbsBasePath()+" Java Version: "+ji.getVersion());
        }
    }
    
}
