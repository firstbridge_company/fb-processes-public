/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.fb.process.examples;

import io.firstbridge.process.db.ConfigProcessor;
import io.firstbridge.process.db.DbControl;
import io.firstbridge.process.db.DbFinders;
import io.firstbridge.process.db.DbRunParams;
import io.firstbridge.process.impl.MariaDbControl;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * This example requires MariaDB installed It runs MariaDB process from
 * installation directory with all data in data directory. If data directory is
 * not "reaady" for MAriaDB, it tries to prepare it.
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class MariaDBRunInstalled {

    static String CONF_NAME = "my-apl-user.cnf";

    private static boolean prepareConfig(Path dbDir, String dataDir, Path confFile) {
        boolean res;
        InputStream confTemplate = Thread.currentThread().getContextClassLoader().getResourceAsStream("conf/my-sys.cnf.template");

        Map<String, String> vars = new HashMap<>();
        vars.put("db_data_dir", dataDir);
        if (!Files.exists(confFile)) {
            System.out.println("Preparing config file from template");
            res = new ConfigProcessor().prepareConfigFile(confTemplate, confFile, vars);
        } else {
            System.out.println("Config file already exists");
            res = true;
        }
        return res;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String HOME = System.getProperty("user.home");

        String MARIA_DATA_DIR = "mariadb_data";
        Path dbDataDir = args.length >= 2 ? Paths.get(args[2]) : Paths.get(MARIA_DATA_DIR);
        Path confFile = Path.of(CONF_NAME);

        DbRunParams params = DbFinders.findISytemWideMariaDb();
        if(params==null){
          System.err.println("Maria DB is not installed or not fpund");  
          System.exit(1);
        }
        params.setDbConfigFile(confFile);
        params.setDbDataDir(dbDataDir);
        params.setOut(Path.of("maria_out.log"));

        DbControl dbControl = new MariaDbControl(params);

        if (!dbControl.findRunning()) {
            System.out.println("MariaDB is not running. Starting with config: " + confFile.toAbsolutePath().toString());
            if (!prepareConfig(params.getDbInstallDir(), dbDataDir.toAbsolutePath().toString(),confFile)){
                System.err.println("Can not write config file");
                System.exit(1);
            }
            if (!dbControl.spawnServer()) {
                System.err.println("Can not start MariaDB, please see logs");
                System.exit(1);
            }
        } else {
            System.out.println("MariaDB is already running. PID=" + dbControl.getdbProcess().getProcessHandle().pid());
        }

        boolean ok = dbControl.isOK();

        if (!ok) {
            System.out.println("MariaDb is OK");
        }
    }
}

