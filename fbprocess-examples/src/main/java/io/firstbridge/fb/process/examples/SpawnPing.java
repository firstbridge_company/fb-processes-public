/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.fb.process.examples;

import io.firstbridge.process.Proc;
import io.firstbridge.process.ProcessExecutor;
import io.firstbridge.process.impl.ProcessExecutorImpl;
import io.firstbridge.process.utils.OsUtils;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * This program spwans "ping" that runs for 50 pings
 * after this program finishes.
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class SpawnPing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ProcessExecutor pexec = new ProcessExecutorImpl();
        List<String> pargs = new ArrayList<>();
        pargs.add("8.8.8.8");
        if(OsUtils.isWindows()){
            pargs.add("-n");
            pargs.add("50");
        } else {
           pargs.add("-c 50");               
        }
        Proc p = pexec.spawn("ping", pargs, null, Paths.get("./"), new File("ping_out"));       
    }
    
}
