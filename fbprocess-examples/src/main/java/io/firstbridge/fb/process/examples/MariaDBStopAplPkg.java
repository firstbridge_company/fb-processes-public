/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.fb.process.examples;

import io.firstbridge.process.db.DbControl;
import io.firstbridge.process.impl.MariaDbControl;
import io.firstbridge.process.impl.MariaDbRunParams;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;

/**
 * This example requires MariaDB Apollo package installed.
 * Please @see https://github.com/ApolloFoundation/dbpackages
 * It runs MariaDB process from
 * installation directory with all data in data directory. If data directory is
 * not "reaady" for MAriaDB, it tries to prepare it.
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class MariaDBStopAplPkg {

    static String CONF_NAME = "my-apl-user.cnf";


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String HOME = System.getProperty("user.home");
        String MARIA_DB_DIR = "ApolloWallet/apollo-mariadb";
        String MARIA_DATA_DIR = "mariadb_data";
        Path dbInstallDir = args.length >= 2 ? Paths.get(args[1]) : Paths.get(HOME, MARIA_DB_DIR);
        Path dbDataDir = args.length >= 3 ? Paths.get(args[2]) : Paths.get(MARIA_DATA_DIR);
        Path confFile = dbDataDir.resolve(CONF_NAME);
        
        MariaDbRunParams params = new MariaDbRunParams();
                params.setDbConfigFile(confFile);
                params.setDbDataDir(dbDataDir);
                params.setDbInstallDir(dbInstallDir);
                params.setOut(Path.of("maria_out.log"));

        if(!params.verify()){
            System.err.println("Please fill dbParams correctly!");
            System.exit(1);
        }
     
        DbControl dbControl = new MariaDbControl(params);

        if (dbControl.findRunning()) {
            System.out.println("MariaDB is running. PID="+dbControl.getdbProcess().getProcessHandle().pid());
            if(!dbControl.stopServer(Duration.ofSeconds(10))){
                System.err.println("Can not stop MariaDB, please see logs");
            }else{
                System.err.println("MariaDB process PID="+dbControl.getdbProcess().getProcessHandle().pid()+" is stopped");                
            }
            
        }else{
            System.out.println("MariaDB is not running.");
        }

    }

}
