/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.fb.process.examples;

import io.firstbridge.process.ProcessExecutor;
import io.firstbridge.process.impl.ProcessExecutorImpl;
import io.firstbridge.process.utils.OsUtils;
import java.nio.file.Path;
import java.util.Arrays;

/**
 * This programs spawns GEdit on Linux or Notepad on Windows
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class SpawnGEdit {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ProcessExecutor pexec = new ProcessExecutorImpl();
        if(OsUtils.isWindows()){
            System.err.println("I doubt there is such program as gedit. Trying to start Notepad");
            pexec.spawn("notepad", Arrays.asList(args), null, Path.of(System.getProperty("user.home")));
        }else{
            pexec.spawn("gedit", Arrays.asList(args), null, Path.of(System.getProperty("user.home")));
        }
    }
    
}
