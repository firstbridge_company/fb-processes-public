# fb-processes #

fb-processes is a library that makes easy to start and control OS processes fromJava code.
Besides it, it can start MariaDB from system-wide installation or from Apollo package

### Modules ###

* [FB-precess library](fbprocess)
* [Examples](fbprocess-examples)
* [Native build with GraalVM example](fbprocess-ngraal)

### Requirements ###

* JDK 11 or GraalVM 20.3.0 or later
* Maven 3.x.x

 No other dependencies

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Oleksiy Lukin alukin@gmail.com