/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.process.conf;

import io.firstbridge.process.conf.VarSubst;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class VarSubstTest {
    Map<String,String> vars = new HashMap<>();
    
    public VarSubstTest() {                
        vars.put("HOME", "/home/al");
        vars.put("USER", "al");
    }


    /**
     * Test of process method, of class VarSubst.
     */
    @Test
    public void testProcess() throws Exception {
        System.out.println("process");
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("test_config.properties");
        OutputStream out = new ByteArrayOutputStream(4096);
        
        VarSubst instance = new VarSubst(vars);
        instance.process(in, out);
        String result = out.toString();
        System.out.println(result);
        String expResult= "#This is test properties file for variable replacement"+System.lineSeparator()+
                "user.home=/home/al"+System.lineSeparator()+
                "user.login=al"+System.lineSeparator()+
                "#end of file"+System.lineSeparator();
        assertEquals(expResult, result);
                
    }

    /**
     * Test of subst method, of class VarSubst.
     */
    @Test
    public void testSubst() {
        System.out.println("subst");
        String in = "user.home=${HOME} user.name=${USER}";
        VarSubst instance = new VarSubst(vars);
        String expResult = "user.home=/home/al user.name=al";
        String result = instance.subst(in);
        assertEquals(expResult, result);

    }
    
}
