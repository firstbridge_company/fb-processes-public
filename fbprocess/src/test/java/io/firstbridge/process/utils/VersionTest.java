/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.utils;

import io.firstbridge.process.utils.Version;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class VersionTest {
    
    private static String[] out_graal = {
        "openjdk version \"11.0.9\" 2020-10-20",
        "OpenJDK Runtime Environment GraalVM CE 20.3.0 (build 11.0.9+10-jvmci-20.3-b06)",
        "OpenJDK 64-Bit Server VM GraalVM CE 20.3.0 (build 11.0.9+10-jvmci-20.3-b06, mixed mode, sharing)"
    };
    
    private static String[] out_openjdk = {
        "openjdk version \"11.0.9.1\" 2020-11-04",
        "OpenJDK Runtime Environment 18.9 (build 11.0.9.1+11)",
        "OpenJDK 64-Bit Server VM 18.9 (build 11.0.9.1+11, mixed mode, sharing"
    };
    
    public VersionTest() {
    }
    /**
     * Test of toString method, of class Version.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Version instance1 = new Version();
        instance1.parse(out_graal[0]);
        String expResult = "11.0.9";
        String result = instance1.toString();
        assertEquals(expResult, result);
        Version instance2 = new Version();
        instance2.parse(out_openjdk[0]);
        expResult = "11.0.9.1";
        result = instance2.toString();
        assertEquals(expResult, result);
        //test compare
        assertTrue(instance2.compareTo(instance1)>0);
    }


    /**
     * Test of getMajor method, of class Version.
     */
    @Test
    public void testGetParts() {
        System.out.println("getVersionParts");
        Version instance = new Version();
        instance.parse(out_graal[0]);
        Integer expResult = 11;
        Integer result = instance.getVersionNumbers().get(0);
        assertEquals(expResult, result);
    }
    
}
