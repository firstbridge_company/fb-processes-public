/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Simple methods for process execution
 * @author Oleksiy Lukin alukin@gmail.com
 */
public interface ProcessExecutor {
    /**
     * Runs process as child and inherits IO streams.
     * All processes executed this way should die when mother process exits.
     * @param exe executable name or path
     * @param args array of command line arguments
     * @param env map of environment variables
     * @param workingDir directory to cd to before start
     * @return process handle
     */
    Proc run(String exe, List<String> args, Map<String,String> env, Path workingDir);
    
    /**
     * Runs process as child and inherits IO streams.All processes executed this way should die when mother process exits.
     * @param exe executable name or path
     * @param args array of command line arguments
     * @param env map of environment variables
     * @param workingDir directory to cd to before start
     * @param input file to read input from
     * @return process handle
     */
    Proc run(String exe, List<String> args, Map<String,String> env, Path workingDir, File input);     
      /**
     * Runs process as child and inherits IO streams.All processes executed this way should die when mother process exits.
     * @param exe executable name or path
     * @param args array of command line arguments
     * @param env map of environment variables
     * @param workingDir directory to cd to before start
     * @param input file to read input from
     * @param output file to redirect error and output streams
     * @return process handle
     */  
    Proc run(String exe, List<String> args, Map<String, String> env, Path workingDir, File input, File output);   
    
    /**
     * Spawns process and redirects IO streams to /dev/null or in some other black hole 
     * All processes executed this way should continue to run when mother process exits.
     * @param exe executable name or path
     * @param args array of command line arguments
     * @param env map of environment variables
     * @param workingDir directory to cd to before start
     * @return process handle
     */ 
    
    Proc spawn(String exe, List<String> args, Map<String,String> env, Path workingDir);
    /**
     * Spawns process and redirects output streams to files 
     * All processes executed this way should continue to run when mother process exits.
     * @param exe executable name or path
     * @param args array of command line arguments
     * @param env map of environment variables
     * @param workingDir directory to cd to before start
     * @param out file for output stream redirection
     * @return process handle
     */     
    Proc spawn(String exe, List<String> args, Map<String,String> env, Path workingDir, File out);
 

}
