/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.db;

import io.firstbridge.process.Proc;
import java.io.OutputStream;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;


/**
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
@Slf4j
public abstract class DbControl {
    protected Proc dbProcess;
    
    protected DbRunParams dbParams;
    
    public DbControl(DbRunParams dbParams) {
        Objects.requireNonNull(dbParams);
        this.dbParams = dbParams;
    }
    
    public abstract boolean isOK();
    
    public Proc getdbProcess(){
        return dbProcess;
    }
    
    public abstract boolean runDbScriptFile(Path script, String dbName, OutputStream out, String dbUser, String dbPassword);
    public abstract boolean runQuery(String script, String dbName, OutputStream out, String dbUser, String dbPassword);
    public  boolean runDbScriptFile(Path script, String dbName, OutputStream out){
        return runDbScriptFile(script, dbName, out, null, null);
    }
    public boolean runQuery(String script, String dbName, OutputStream out){
        return runQuery(script, dbName, out, null, null);
    }
    public abstract boolean spawnServer();
    public abstract boolean findRunning();
    protected abstract boolean firstStart();    

    public abstract boolean stopServer(Duration timeout);
}
