/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.db;

import io.firstbridge.process.conf.VarSubst;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author leskiy lukin alukin@gmail.com
 */
@Slf4j
public class ConfigProcessor {
    
    public boolean prepareConfigFile(InputStream confTemplate, Path confFile, Map<String,String> vars){
        boolean res = true;
        try {
            VarSubst vs = new VarSubst(vars);
            FileOutputStream out = new FileOutputStream(confFile.toFile());
            vs.process(confTemplate, out);
        } catch (IOException ex) {
           log.error("File can not be written: {}",confFile.toAbsolutePath());
        }
        return res;        
    }
    
    public  boolean prepareConfigFile(Path confTemplate, Path confFile, Map<String,String> vars){
        FileInputStream in;
        boolean res;
        try {
            in = new FileInputStream(confTemplate.toFile());
            res = prepareConfigFile(in, confFile, vars);
        } catch (FileNotFoundException ex) {
           log.error("Template file not found.", ex);
           res = false;
        }
        return res;
    }    
}
