/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.db;

import io.firstbridge.process.conf.VarSubst;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Parameters needed to run DB server
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
@Slf4j
public class DbRunParams {

    public static final String DB_INIT_SCRIPT_TEMPLATE = "conf/my_create_user.sql.template";
    public static final String DB_CONFIG_TEMPLATE = "conf/my_apl-cnf.template";
    @Getter
    @Setter
    Path dbInstallDir;
    @Getter
    @Setter
    Path dbClientExe;
    @Getter
    @Setter
    Path dbDExe;
    @Getter
    @Setter
    Path dbDataDir;
    @Getter
    @Setter
    Path out;
    @Getter
    @Setter
    String dbUser;
    @Getter
    @Setter
    String dbPassword;
    @Getter
    @Setter
    String dbName;
    @Getter
    @Setter        
    Path dbConfigFile;
    @Getter
    Path dbInitScript;
    @Getter
    @Setter        
    Path dbConfigFileTemplate;
    @Getter
    @Setter        
    Path dbInitScriptTemplate;
    @Getter
    @Setter        
    Map<String, String> varSubstMap;
    @Getter
    @Setter
    Path dbInstallExe;

    public DbRunParams() {
    }

    public boolean verify() {
        boolean res
                = dbClientExe != null
                && dbDExe != null
                && dbDataDir != null
                && dbConfigFile != null;
        return res;
    }

    public boolean processConfigTemplates() {
        boolean res = false;
        
        if (Files.exists(dbConfigFile)) {
            log.warn("Database config file already exists");
            return true;
        }
        if (varSubstMap == null || varSubstMap.isEmpty()) {
            log.error("Please fill variable substitution map with appropriavte data");
            return res;
        }
        if(dbConfigFile==null){
            log.error("Please specify database config file location");
            return res;
        }
        if(dbInitScript==null){
            try {            
                dbInitScript=Files.createTempFile("mariadb-init", ".sql");
            } catch (IOException ex) {
             //tmp file must be created anyway
            }
            log.warn("Database init script path is not set, using temp file: {}",dbInitScript.toAbsolutePath().toString());
        }
        res =  subsTemplate(dbConfigFileTemplate, DB_CONFIG_TEMPLATE, dbConfigFile);
        res =  res && subsTemplate(dbInitScriptTemplate, DB_INIT_SCRIPT_TEMPLATE , dbInitScript);
        
        return res;
    }
    
    private boolean subsTemplate(Path templ, String templInResources, Path out){
        boolean res = true;
        VarSubst subst = new VarSubst(varSubstMap); 
        InputStream ctis;
        OutputStream cout;
        try {
            if (templ == null) {
                ctis = Thread.currentThread().getContextClassLoader().getResourceAsStream(templInResources);
                log.info("Processing template from resources: {}",templInResources);
            } else {
                ctis = new FileInputStream(templ.toFile());
            }
            if(!Files.exists(out.getParent())){
                out.getParent().toFile().mkdirs();
            }
            cout = new FileOutputStream(out.toFile());
            subst.process(ctis, cout);
            ctis.close();
            cout.close();
        } catch (IOException ex) {
           log.error("Error processing template file", ex);
           res=false;
        }         
         return res;
    }
}
