/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.db;

import io.firstbridge.process.utils.OsUtils;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Finds installed DB servers
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class DbFinders {
    public static String MariaDBVersion = "10.5";
    public static Path winMariaDBBase = Path.of("Program Files").resolve("MariaDB "+MariaDBVersion);
        
    public static DbRunParams findISytemWideMariaDb() {
        DbRunParams db = new DbRunParams();
        
        switch (OsUtils.whichOS()) {
            case LINUX:
                db.dbDExe = Path.of("/usr/libexec/mariadbd");
                db.dbClientExe = Path.of("/usr/bin/mariadb");
                db.dbInstallExe = Path.of("/usr/bin/mariadb-install-db");
                // db.dbInstallDir =  Path.of("/usr");
                break;
            case WINDOWS:
                db.dbInstallDir =  winMariaDBBase;
                db.dbDExe = winMariaDBBase.resolve("bin").resolve("mariadbdd.exe");
                db.dbDExe = winMariaDBBase.resolve("bin").resolve("mariadbd.exe");
                db.dbInstallExe =winMariaDBBase.resolve("bin").resolve("mariadb-install-db.exe");
                break;
            case MAC_OS:
                db.dbDExe = Path.of("/usr/local/bin/mariadbd");
                db.dbDExe = Path.of("/usr/local/bin/mariadb");
                db.dbInstallExe = Path.of("/usr/local/bin/mysql-install-db");
                break;
            case BSD:
                db.dbDExe = Path.of("/usr/local/libexex/mariadbd");
                db.dbDExe = Path.of("/usr/local/bin/mariadb");
                db.dbInstallExe = Path.of("/usr/local/bin/mariadb-install-db");
            default:
                db.dbDExe = Path.of("/usr/libexec/mariadbd");
                db.dbClientExe = Path.of("/usr/bin/mariadb");
                db.dbInstallExe = Path.of("/usr/bin/mariadb-install-db");
        }
        
        if (
                Files.exists(db.dbDExe)
                && Files.isExecutable(db.dbDExe)
                && Files.exists(db.dbClientExe)
                && Files.exists(db.dbInstallExe)
               // && Files.exists(db.dbInstallDir)
            ) 
        {
            return db;
        }else{
            return null;
        }
    }
}
