/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.conf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class VarSubst {

    private final Map<String, String> vars;
    private final Pattern variablePattern = Pattern.compile("\\$\\{(.+?)\\}");

    public VarSubst(Map<String, String> vars) {
        this.vars = vars;
    }

    public void process(InputStream in, OutputStream out) throws IOException {
        BufferedReader isr = new BufferedReader(new InputStreamReader(in));
        BufferedWriter osw = new BufferedWriter(new OutputStreamWriter(out));
        String line;
        while ((line = isr.readLine()) != null) {
            String outs = subst(line);
            osw.write(outs);
            osw.write(System.lineSeparator());
        }
        osw.flush();
    }

    public String subst(String in) {
        Matcher matcher = variablePattern.matcher(in);
        StringBuilder output = new StringBuilder();
        
        int lastStart = 0;
        while (matcher.find()) {
            String subString = in.substring(lastStart, matcher.start());
            String varName = matcher.group(1);
            String replacement = vars.get(varName);
            if(replacement==null){
                replacement=varName+"_IS_NOT_SET";
            }
            output.append(subString).append(replacement);
            lastStart = matcher.end();
        }
        output.append(in.substring(lastStart));
        return output.toString();
    }
}
