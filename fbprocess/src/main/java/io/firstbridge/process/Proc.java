/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process;

import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Representss OS process
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class Proc {

    private ProcessHandle process;
    private Process p;

    public Proc() {
    }

    public Proc(Process p) {
        if (p != null) {
            process = p.toHandle();
            this.p = p;
        }
    }

    public boolean isValid() {
        return process != null;
    }

    public Proc(ProcessHandle ph) {
        process = ph;
    }

    public boolean terminate() {
        if (!isValid()) {
            return true;
        }
        return process.destroyForcibly();
    }

    public boolean waitForExit(Duration timeout) throws InterruptedException {
        boolean res = true;
        if (!isValid()) {
            return res;
        }
        boolean done = false;
        long ms = timeout.toMillis();
        while (!done) {
            if (!process.isAlive()) {
                done = true;
            } else {
                Thread.sleep(1);
                ms--;
                done = (ms <= 0);
            }
        }
        return res;
    }

    public boolean stop(Duration timeout) {
        boolean res = true;
        try {

            if (!isValid()) {
                return res;
            }
            process.destroy();
            waitForExit(timeout);
            if (process.isAlive()) {
                process.destroyForcibly();
                res = false;
            }

        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return res;
    }

    public boolean isRunning() {
        return (isValid() && process.isAlive());
    }

    public int getReturnCode() {
        if (p != null) {
            return p.exitValue();
        }
        return 0;
    }

    public ProcessHandle getProcessHandle() {
        return process;
    }

    public Process getProcess() {
        return p;
    }

}
