/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.impl;

import io.firstbridge.process.db.DbRunParams;
import io.firstbridge.process.utils.OsUtils;
import java.nio.file.Path;

/**
 *
 * @author @author Oleksiy Lukin alukin@gmail.com
 */
public class MariaDbRunParams extends DbRunParams {

 
    @Override
    public void setDbInstallDir(Path dbInstallDir) {
         super.setDbInstallDir(dbInstallDir);
         Path binDir = getDbInstallDir().resolve("bin");
         if (OsUtils.isWindows()) {
            setDbInstallExe(binDir.resolve("mariadb-install-db.exe"));
            setDbClientExe(binDir.resolve("mariadb.exe"));
            setDbDExe(binDir.resolve("mariadbd.exe"));
        } else {
            setDbInstallExe(getDbInstallDir().resolve("scripts").resolve("mariadb-install-db"));
            setDbClientExe(binDir.resolve("mariadb"));
            setDbDExe(binDir.resolve("mariadbd"));
        }      
    }

}
