/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.impl;

import io.firstbridge.process.Proc;
import io.firstbridge.process.ProcessExecutor;
import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Executes Jar file using defined JRE
 * @author Oleksiy Lukin alukin@gmail.com
 */
@Slf4j
public class JarExecutor implements ProcessExecutor{
    @Getter
    private final Path jreBase;
    private final String java;
    private final ProcessExecutor executor = new ProcessExecutorImpl();
    
    public JarExecutor(Path jreBase) {
        this.jreBase = jreBase;
        java = jreBase.resolve("bin").resolve("java").toAbsolutePath().toString();
    }

    @Override
    public Proc run(String exeJar, List<String> args, Map<String, String> env, Path workingDir) {
        args.add(0, exeJar);
        args.add(0, "-jar");
        return executor.run(java, args, env, workingDir);
    }

    @Override
    public Proc run(String exeJar, List<String> args, Map<String, String> env, Path workingDir, File input) {
        args.add(0, exeJar);
        args.add(0, "-jar");
        return executor.run(java, args, env, workingDir, input);
    }
    
    @Override
    public Proc spawn(String exeJar, List<String> args, Map<String, String> env, Path workingDir) {
        args.add(0, exeJar);
        args.add(0, "-jar");
        return executor.spawn(java, args, env, workingDir);
    }

    @Override
    public Proc spawn(String exeJar, List<String> args, Map<String, String> env, Path workingDir,  File out) {
        args.add(0, exeJar);
        args.add(0, "-jar");
        return executor.spawn(java, args, env, workingDir, out);
    }

    @Override
    public Proc run(String exeJar, List<String> args, Map<String, String> env, Path workingDir, File input, File output) {
        args.add(0, exeJar);
        args.add(0, "-jar");
        return executor.run(java, args, env, workingDir,input,output);
        
    }
    
}
