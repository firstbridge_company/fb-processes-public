/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.impl;

import io.firstbridge.process.ProcessExecutor;
import io.firstbridge.process.Proc;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
@Slf4j
public class ProcessExecutorImpl implements ProcessExecutor {

    public ProcessBuilder makeBuilder(String exe, List<String> args, Map<String, String> env, Path workingDir) {
        List<String> command = new ArrayList<>();
        command.add(exe);
        command.addAll(args);

        ProcessBuilder pb = new ProcessBuilder(command);
        if(workingDir!=null){
                pb.directory(workingDir.toFile());
        }

        if (!(env == null || env.isEmpty())) {
            pb.environment().putAll(env);
        }
        
        return pb;
    }


    @Override
    public Proc run(String exe, List<String> args, Map<String, String> env, Path workingDir) {
        return run(exe, args, env, workingDir, null, null);
    }
    
    @Override
    public Proc run(String exe, List<String> args, Map<String, String> env, Path workingDir, File input) {
        return run(exe, args, env, workingDir, input, null);
    }
    
    @Override
    public Proc run(String exe, List<String> args, Map<String, String> env, Path workingDir, File input, File output) {
        ProcessBuilder pb = makeBuilder(exe, args, env, workingDir);
        if(input!=null){
            pb.redirectInput(ProcessBuilder.Redirect.from(input));
        }else{
            pb.redirectInput(ProcessBuilder.Redirect.INHERIT);
        }
        if(output!=null){
            pb.redirectErrorStream(true);
            pb.redirectOutput(ProcessBuilder.Redirect.appendTo(output));
        }else{
            pb.redirectOutput(java.lang.ProcessBuilder.Redirect.INHERIT);
            pb.redirectError(java.lang.ProcessBuilder.Redirect.INHERIT);
        }
        Process p = null;
        try {
            p = pb.start();
        } catch (IOException ex) {
            log.error("IO exception while executing "+exe,ex);
        }
        Proc res = new Proc(p);
        return res;
    }

    @Override
    public Proc spawn(String exe, List<String> args, Map<String, String> env, Path workingDir) {
        ProcessBuilder pb = makeBuilder(exe, args, env, workingDir);
        pb.redirectError(ProcessBuilder.Redirect.DISCARD);
        pb.redirectOutput(ProcessBuilder.Redirect.DISCARD);
        Process p = null;
        try {
            p = pb.start();
        } catch (IOException ex) {
            log.error("Can not spawn process", ex);
        }
        Proc res = new Proc(p);

        return res;
    }

    @Override
    public Proc spawn(String exe, List<String> args, Map<String, String> env, Path workingDir,  File out) {
        ProcessBuilder pb = makeBuilder(exe, args, env, workingDir);
        pb.redirectErrorStream(true);
        pb.redirectOutput(ProcessBuilder.Redirect.appendTo(out));        
        Process p = null;
        try {
            p = pb.start();            
        } catch (IOException ex) {
           log.error("Can not spawn process: "+ex);
        }
        Proc res = new Proc(p);
        return res;
    }

}
