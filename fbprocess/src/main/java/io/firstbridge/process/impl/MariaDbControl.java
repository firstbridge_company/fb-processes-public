/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.impl;

import io.firstbridge.process.Proc;
import io.firstbridge.process.ProcessControl;
import io.firstbridge.process.ProcessExecutor;
import io.firstbridge.process.conf.IniFile;
import io.firstbridge.process.conf.VarSubst;
import io.firstbridge.process.db.DbControl;
import io.firstbridge.process.db.DbRunParams;
import io.firstbridge.process.utils.OsUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ProcessHandle.Info;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
@Slf4j
public class MariaDbControl extends DbControl {

    private static final ProcessExecutor pe = new ProcessExecutorImpl();

    public MariaDbControl(DbRunParams dbParams) {
        super(dbParams);
        dbParams.setDbName("mysql");
        // user?
    }

    @Override
    public boolean isOK() {
        boolean res = false;
        
        if (dbProcess!=null && dbProcess.isRunning()) {
            res=true;
        }
        return res;
    }


    @Override
    public boolean runDbScriptFile(Path script, String dbName, OutputStream out, String dbUser, String dbPassword) {
        //mariadb --database dbName --batch < script > out
        boolean res;
        Path exe = dbParams.getDbClientExe();
        List<String> args = new ArrayList<>();
        args.add("--defaults-file="+dbParams.getDbConfigFile().toAbsolutePath().toString());
        if(dbName==null){
            dbName=dbParams.getDbName();
        }
        args.add("--database="+dbName);
        if(OsUtils.isWindows()){ //on windows we have to use "root" user without password as default
           if(dbUser==null || dbUser.isEmpty()){
               dbUser="root";
           } 
        }
        if(dbUser!=null && !dbUser.isEmpty()){
            args.add("--user="+dbUser);        
        }
        if(dbPassword!=null && !dbPassword.isEmpty()){
            args.add("--password="+dbPassword);
        }
        Proc proc = pe.run(exe.toAbsolutePath().toString(), args, null, null,script.toAbsolutePath().toFile());
        
        try {
            proc.getProcess().waitFor();
        } catch (InterruptedException ex) {
            log.warn("First run of MariaDB installation is interruped!");
        }
        res = proc.getReturnCode() == 0;
        if(!res){
            log.error("Script failed: {}",script.toAbsolutePath().toString());
        }
        return res;
    }
    
    @Override
    public boolean runQuery(String script, String dbName, OutputStream out, String dbUser, String dbPassword) {
        boolean res = false;
        try{ 
            File tempFile = File.createTempFile("mariddb-script-", "sql");
            try (FileWriter writer = new FileWriter(tempFile)) {
                writer.write(script);
            }
            Path p = Path.of(tempFile.getAbsolutePath());
            res =  runDbScriptFile(p,dbName,out, dbUser,dbPassword);
            tempFile.delete();
        }catch(IOException ex){
            log.error("Can not create temp file", ex);
        }
        return res;
    }

    @Override
    public boolean spawnServer() {
        boolean res = false;
        boolean isFisrstStart;
        
        if (!Files.exists(dbParams.getDbDExe())) {
            log.error("MaridDB daemon executaable not found");
            return res;
        }
        isFisrstStart = !Files.exists(dbParams.getDbDataDir().resolve("data"));
        if (isFisrstStart) {
           if(!firstStart()){
               log.error("First DB start procedure failed!");
               return res;
           }
        }

       
        List<String> args = new ArrayList<>();
        args.add("--defaults-file=" + dbParams.getDbConfigFile().toAbsolutePath());
        args.add("--verbose");
        dbProcess = pe.spawn(
                dbParams.getDbDExe().toAbsolutePath().toString(), 
                args, 
                null, 
                dbParams.getDbDataDir(), 
                dbParams.getOut().toFile()
        );
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        res = dbProcess.isRunning();
        if(isFisrstStart && res ){
           //user is null because we just connect with derfault user, which is the user
           //mariadbd process is starte under
           res = runDbScriptFile(prepareInitScript(), dbParams.getDbName(), null);
        }
        return res;
    }

    @Override
    public boolean findRunning() {
        boolean res = false;
        ProcessControl pcontr = new ProcessControlImpl();
        List<Proc> dbs = pcontr.findRunning(dbParams.getDbDExe().toString());
        if (dbs.size() >= 1) {
            Proc dbProc = dbs.get(0);
            Info info = dbProc.getProcessHandle().info();
            if(info.arguments().isPresent()){
                String[] args = info.arguments().get();
                //TODO: comman dline args?
            }
            //TODO
            // is it what we need? We may have several instances
            dbProcess = dbProc;
            res = true;
        }
        return res;
    }
    
    private boolean firstStartWindows(){
        boolean res;
        List<String> args = new ArrayList<>();
        IniFile mariaConf = new IniFile(dbParams.getDbConfigFile().toAbsolutePath().toString());
        
        if(!mariaConf.load()){
            return false;
        }
        String  exe = dbParams.getDbInstallExe()
                      .toAbsolutePath()
                      .toString();
        args.add("--datadir=" + dbParams.getDbDataDir().resolve("data").toAbsolutePath());
        args.add("--port=" + mariaConf.getString("client-server", "port", "3366"));
        Proc proc = pe.run(exe, args, null, null);
        try {
            proc.getProcess().waitFor();
        } catch (InterruptedException ex) {
            log.warn("First run of MariaDB installation is interruped!");
        }
        OsUtils.copyFile(dbParams.getDbConfigFile(),dbParams.getDbDataDir());
        res = proc.getReturnCode() == 0;
       
        return res;
    }
    
    private Path prepareInitScript(){
        Path res = null;
        try {
            InputStream template = Thread.currentThread().getContextClassLoader().getResourceAsStream("conf/my_create_user.sql.template");
            File tempFile = File.createTempFile("mariddb-create-user-", "sql");
            tempFile.deleteOnExit();
            res  = Path.of(tempFile.getAbsolutePath());
            Map<String,String> vars = new HashMap<>();
            vars.put("dbuser", dbParams.getDbUser());
            vars.put("dbuser_password", dbParams.getDbPassword());
            VarSubst vs = new VarSubst(vars);            
            vs.process(template, new FileOutputStream(tempFile));
        } catch (FileNotFoundException ex) {
            log.error("Can not create tmp file", ex);
        } catch (IOException ex) {
            log.error("Can not write to tmp file", ex);
        }
        return res;
    }
    
    @Override
    protected boolean firstStart() {
        boolean res;
        dbParams.getDbDataDir().toFile().mkdirs();
        dbParams.getDbDataDir().resolve("data").toFile().mkdir();
        dbParams.getDbDataDir().resolve("tmp").toFile().mkdir();

        if(OsUtils.isWindows()) {
            return firstStartWindows();
        }
        
        List<String> args = new ArrayList<>();        
        String      exe = "/bin/sh";
            args.add(dbParams.getDbInstallExe()
                    .toAbsolutePath()
                    .toString());
        
        if(dbParams.getDbInstallDir()!=null) { // we do not need it if MariaDB installed system-wide
           args.add("--basedir=" + dbParams.getDbInstallDir());
        }
        args.add("--datadir=" + dbParams.getDbDataDir().resolve("data").toAbsolutePath());
        args.add("--defaults-file=" +dbParams.getDbConfigFile().toAbsolutePath());
        args.add("--skip-test-db");
        Proc proc = pe.run(exe, args, null, null, null, dbParams.getOut().toFile());
        try {
            proc.getProcess().waitFor();
        } catch (InterruptedException ex) {
            log.warn("First run of MariaDB installation is interruped!");
        }
        res = proc.getReturnCode() == 0;
        if(!res){            
            log.error("Can not init MariaDB data dir. Please remove directory, fix errors and try again");
        }
        return res;
    }

    @Override
    public boolean stopServer(Duration timeout) {
        boolean res = false;
        if(dbProcess!=null){
            res = dbProcess.stop(timeout);
        }
        return res;
    }

}
