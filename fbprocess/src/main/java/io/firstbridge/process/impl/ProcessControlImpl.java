/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.impl;

import io.firstbridge.process.Proc;
import io.firstbridge.process.ProcessControl;
import java.lang.ProcessHandle.Info;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class ProcessControlImpl implements ProcessControl{

    @Override
    public List<Proc> findRunning(String exePath) {
        List<Proc> res = new ArrayList<>();
        ProcessHandle.allProcesses().forEach(ph->{
            Info i = ph.info();
            if(i.command().isPresent()){
              if(exePath.equals(i.command().get())){
                  Proc p = new Proc(ph);
                  res.add(p);
              }
            }
        });
        return res;
    }
    
}
