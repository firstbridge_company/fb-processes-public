/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Parses string to find version pattern
 * Outputs version as string
 * Supported formats: x.y.z; x.y.z.v
 * @author Oleksiy Lukin alukin@gmail.com
 */
@NoArgsConstructor
@AllArgsConstructor
public class Version implements Comparable{
    @Getter
    @Setter
    private List<Integer> versionNumbers = new ArrayList<>();
    @Getter
    @Setter
    private String suffix="";
    
    private static final Pattern VERSIONNUMBER = Pattern.compile("((\\d+)((\\.\\d+)+)?)");
    private static final Pattern VERSIONNUMBER_WITH_SUFFIX = Pattern.compile(VERSIONNUMBER.pattern() + "((\\s|\\-|\\.|\\[|\\]|\\w+)+)?");
    

    @Override
    public String toString(){
       String res=""; 
       for( Integer vn: versionNumbers){
           if(!res.isEmpty()){
               res+=".";
           }
           res=res+vn;
       } 

       if(!suffix.isEmpty()){
            res=res+"-"+suffix;
       }

       return res;
    }
    
    public void parseVersion(String pureVersion){
        versionNumbers.clear();
        while(!pureVersion.isEmpty()){
            int pos = pureVersion.indexOf(".");
            if(pos>=0){
               String digit = pureVersion.substring(0,pos);
               pureVersion = pureVersion.substring(pos+1,pureVersion.length());
               versionNumbers.add(Integer.parseInt(digit));
            }else{
               versionNumbers.add(Integer.parseInt(pureVersion));
               pureVersion="";
            }
        }
        
    }
    
    public boolean parse(String versionString){
        boolean res = false;
        Pattern pattern = VERSIONNUMBER_WITH_SUFFIX;
        Matcher m = pattern.matcher(versionString);
        if(m.find()){
            res = true;
            String ver = m.group(0);
            parseVersion(ver);
            //TODO: read suffix
            // suffix = m.group(2);
        }
            
        return res;
    }
    
    private int pow(int x, int n){
        int res=1;
        for (int i = 1; i <= n; i++) {
            res *= x;
        }
        return res;        
    }
    
    private int variantToInt(){
       int res = 0;
       try{
        res = Integer.parseInt(suffix);
       }catch(NumberFormatException ex){           
       }
       return res;
    }
    
//only 4 digits matter
    private int calcBigV(List<Integer> vn){
        int big = 0;
        List<Integer> l = new ArrayList<>();
        l.addAll(vn);
        while(l.size()<4){
            l.add(0);
        }
    
        for(int i=0; i<4; i++){
            big += l.get(i) * pow(10,(4-i));
        }
        
        return big;    
    }  
    
    @Override
    public int compareTo(Object o) {
        if ( o instanceof Version){
           Version v = (Version)o;
           return calcBigV(versionNumbers)-calcBigV(v.versionNumbers);
        }else{
            return -1;
        }
    }
    
}
