/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
@Slf4j
public class OsUtils {

    public enum OS_FAMILY {
        LINUX,
        WINDOWS,
        MAC_OS,
        BSD,
        OTHER
    }

    /**
     * True if we are running on Windows
     *
     * @return true if on Windows, fals if on some POSIX compatible OS
     */
    public static boolean isWindows() {
        String os = System.getProperty("os.name");
        return os.startsWith("Windows");
    }

    public static boolean isLinux() {
        String os = System.getProperty("os.name");
        return os.startsWith("Linux");
    }

    public static boolean isMacOS() {
        String os = System.getProperty("os.name");
        return os.startsWith("Mac OS");
    }

    public static boolean isBSD() {
        String os = System.getProperty("os.name");
        return os.contains("BSD");
    }

    public static OS_FAMILY whichOS() {
        OS_FAMILY res = OS_FAMILY.OTHER;
        if (isWindows()) {
            res = OS_FAMILY.WINDOWS;
        } else if (isLinux()) {
            res = OS_FAMILY.LINUX;
        } else if (isMacOS()) {
            res = OS_FAMILY.MAC_OS;
        } else if (isBSD()) {
            res = OS_FAMILY.BSD;
        }

        return res;
    }

    public static List<String> readEnvPath() {
        Map<String, String> env = System.getenv();
        List<String> res = new ArrayList<>();
        res.addAll(Arrays.asList(env.get("PATH").split(File.pathSeparator)));
        return res;
    }

    public static void removeRecursively(Path path) throws IOException {
        if (!Files.exists(path)) {
            return;
        }
        if (!Files.isDirectory(path)) {
            Files.delete(path);
        } else {
            Files.walkFileTree(path,
                    new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult postVisitDirectory(
                        Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(
                        Path file, BasicFileAttributes attrs)
                        throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }
    
    public static Path userHome(){
        String home = System.getProperty("user.home");
        Path res = Path.of(home).toAbsolutePath();
        return res;
    }

    public static boolean copyFile(Path srcFile, Path dstFile) {
       boolean res = false;
       Path dst;
       if(!Files.exists(srcFile)){
           return res;
       }
       if(Files.isDirectory(dstFile)){
           dst = dstFile.resolve(srcFile.getFileName());
       }else{
           dst = dstFile;
       }
       Path dstDir = dst.getParent(); 
       if(!Files.exists(dstDir)){
           dstDir.toFile().mkdirs();
       }
        try {
            Files.copy(srcFile,dst,StandardCopyOption.REPLACE_EXISTING);
            res=true;
        } catch (IOException ex) {
            log.warn("Can not copy file {} to {}.",srcFile,dstFile,ex);
        }
        return res;
    }
    
}
