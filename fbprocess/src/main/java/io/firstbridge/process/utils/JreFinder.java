/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

/**
 * Tries to find installed JREs
 *
 * @author Oleksiy Lukin alukin@gmail.com
 */
@Slf4j
public class JreFinder {

    public Set<String> proposeSearchDirs(){
        Set<String> searchDirs = new HashSet<>();
        if (!OsUtils.isWindows()) {
            Path sdkman = OsUtils.userHome().resolve(".sdkman");
            if (Files.exists(sdkman)) {
                Path javas = sdkman.resolve("candidates").resolve("java");
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(javas)) {
                    for (Path path : stream) {
                        if (Files.isDirectory(path)) {
                            searchDirs.add(
                                    path
                                    .toRealPath()
                                    .resolve("bin")
                                    .toAbsolutePath()
                                    .toString()
                            );
                        }
                    }
                }catch(IOException ex){                    
                }
            }
        } else {
            //TODO
            //is there some SdkMan on Windows or other preffered ways to install Java?
        }
        searchDirs.addAll(OsUtils.readEnvPath());
        return searchDirs;
    }

    public Set<Path> findJREs(Set<String> searchDirs) {
        Set<Path> jres = new HashSet<>();
        for (String s : searchDirs) {
            Path fp = Paths.get(s, "java");
            if (Files.exists(fp) && Files.isExecutable(fp)) {
                try {
                    fp = fp.toRealPath();
                } catch (IOException ex) {
                    log.warn("Can not resolve symlink: {}", fp.toString());
                    continue;
                }
                jres.add(fp.toAbsolutePath().getParent().getParent());
            }
        }
        return jres;
    }

    public JreInfo getInfo(Path baseDir) {
        JreInfo info = null;
        File outFile;
        try {
            outFile = Files.createTempFile("jrefinder", ".out").toFile();

            Path java = baseDir.resolve("bin").resolve("java");
            List<String> command = new ArrayList<>();
            command.add(java.toString());
            command.add("-version");
            ProcessBuilder pb = new ProcessBuilder(command)
                    .redirectErrorStream(true)
                    .redirectOutput(outFile);
            Process p = pb.start();
            int exitcode = p.waitFor();
            if (exitcode == 0) { // it is java VM
                List<String> out = Files.readAllLines(outFile.toPath());
                info = JreInfo.fromVersionOutput(out, baseDir);
            }
        } catch (IOException ex) {
            log.error("Can not create temp file", ex);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return info;
    }

    public List<JreInfo> findWithInfo(Set<String> searchDirs) {
        List<JreInfo> res = new ArrayList<>();
        findJREs(searchDirs).stream().map(p -> getInfo(p)).forEachOrdered(i -> {
            res.add(i);
        });
        return res;
    }
}
