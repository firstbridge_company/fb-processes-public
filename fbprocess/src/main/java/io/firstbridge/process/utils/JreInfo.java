/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firstbridge.process.utils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import lombok.Getter;

/**
 * 
 * @author Oleksiy Lukin alukin@gmail.com
 */
public class JreInfo {
    @Getter
    private Path absBasePath;
    @Getter
    private final Version version = new Version();
    @Getter
    private String info;
    @Getter
    private String vendor;
    @Getter   
    private boolean isJDK;
    @Getter
    private boolean isGraalVM;
    
    public static JreInfo fromVersionOutput(List<String> out, Path basePath){
        JreInfo ji = new JreInfo();
        Path javac = basePath.resolve("bin").resolve("javac");
        ji.isJDK = Files.exists(javac) && Files.isExecutable(javac);
        
        for(String s: out){
           if(ji.version.parse(s)){
               break;
           }    
        }
        ji.absBasePath = basePath.toAbsolutePath();
        //TODO: rest of fields
        return ji;
    }
}
