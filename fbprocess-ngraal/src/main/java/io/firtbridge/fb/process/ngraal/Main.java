/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
package io.firtbridge.fb.process.ngraal;

import io.firstbridge.process.Proc;
import io.firstbridge.process.ProcessExecutor;
import io.firstbridge.process.impl.ProcessExecutorImpl;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *  @author Oleksiy Lukin alukin@gmail.com
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        ProcessExecutor pexec = new ProcessExecutorImpl();
        List<String> pargs = new ArrayList<>();
        pargs.add("-v");
        pargs.add("8.8.8.8");
        Proc p = pexec.run("ping", pargs, null, Paths.get("./"));
        Thread.sleep(4000);
        p.stop(Duration.ofSeconds(1));
    }
    
}
